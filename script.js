'use strict'

localStorage.getItem('theme') ? false : localStorage.setItem('theme', 'oldTheme');

window.addEventListener('load', () => {

    let currentTheme = localStorage.getItem('theme');

    const oldTheme = {
        NavBg : '#35444F',
        backgroundC : '#fff',
        btnC : '#cc3c1c'
    }

    const newTheme = {
        NavBg : '#19e10a',
        backgroundC : 'rgba(99, 105, 110, 0.38)',
        btnC : '#19e10a'
    }
    currentTheme === 'oldTheme' ? applyStyles(oldTheme) : applyStyles(newTheme);

    document.querySelector('.theme-btn').addEventListener('click', changeTheme);

    function changeTheme() {
        let currTheme = localStorage.getItem('theme');
        if (currTheme === 'oldTheme') {
            localStorage.setItem('theme', 'newTheme');
            applyStyles(newTheme);
        } else {
            localStorage.setItem('theme', 'oldTheme');
            applyStyles(oldTheme);
        }
    }

    function applyStyles(themeName) {
        document.querySelector('.nav_top').style.backgroundColor = themeName.NavBg;
        document.querySelector('.bottom').style.backgroundColor = themeName.NavBg;
        document.querySelector('.theme-btn').style.background = themeName.btnC;
        document.querySelector("body").style.background = themeName.backgroundC;
    }
})






















